# Test for [`trigger:forward:yaml_variables`](https://docs.gitlab.com/ee/ci/yaml/#triggerforward)

I expect that when `trigger:forward:yaml_variables` is `false`:
* globally defined YAML variables _should not_ be defined in child pipelines. This currently works as expected.
* job defined YAML variable _should_ be defined in child pipelines. This currently _does not_ work as expected.
